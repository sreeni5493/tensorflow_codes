import tensorflow as tf
import numpy as np
from PIL import Image
from skimage import transform
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
import os
Image.MAX_IMAGE_PIXELS = None
train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1.0/255)
test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1.0/255)
batch_size = 64
train_dir = r'D:\images_sample_set_curated\training_set'
test_dir = r'D:\images_sample_set_curated\testing_set'
test_failed_dir = r'D:\images_sample_set_curated\failed_test_samples'
train_failed_dir = r'D:\images_sample_set_curated\failed_train_samples'
sub_folders = ['artwork', 'leaflet']
training_set = train_datagen.flow_from_directory(train_dir, target_size=(500, 500), batch_size=batch_size,
                                                 color_mode='rgb', class_mode='binary', shuffle=True)

test_set = test_datagen.flow_from_directory(test_dir, target_size=(500, 500), batch_size=batch_size, color_mode='rgb',
                                            class_mode='binary', shuffle=False)

print(training_set)
print(test_set)

# Initialising the CNN
classifier = tf.keras.models.Sequential()

# Step 1 - Convolution
classifier.add(tf.keras.layers.Conv2D(32, (3, 3), input_shape=(500, 500, 3)))
classifier.add(tf.keras.layers.Activation("relu"))
classifier.add(tf.keras.layers.MaxPooling2D(pool_size=(3, 3)))
classifier.add(tf.keras.layers.Conv2D(64, (3, 3), input_shape=(500, 500, 3)))
classifier.add(tf.keras.layers.Activation("relu"))
classifier.add(tf.keras.layers.MaxPooling2D(pool_size=(3, 3)))

classifier.add(tf.keras.layers.Flatten())

classifier.add(tf.keras.layers.Dense(64))
classifier.add(tf.keras.layers.Activation("relu"))
classifier.add(tf.keras.layers.Dense(128))
classifier.add(tf.keras.layers.Activation("relu"))
classifier.add(tf.keras.layers.Dense(activation='sigmoid', units=1))
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
print(classifier.summary())
classifier.fit_generator(training_set, steps_per_epoch=np.ceil(training_set.samples / batch_size), epochs=20,
                         validation_steps=np.ceil(test_set.samples / batch_size), validation_data=test_set)
classifier.save('updated_model.h5')
# https://www.kaggle.com/menaceslinger/keras-beginner-cats-vs-dogs-classification

probabilities = classifier.predict(test_set)
actual_labels = test_set.filenames
actual_label_values = []
for label in actual_labels:
    if sub_folders[0] in label:
        actual_label_values.append(0)
    elif sub_folders[0] in label:
        actual_label_values.append(1)
prob_binary = probabilities > 0.5
prob_binary = list(prob_binary)
cm = confusion_matrix(actual_label_values, prob_binary)
accuracy = accuracy_score(actual_label_values, prob_binary)
print('Confusion matrix test set:\n', cm)
print('\nAccuracy on test set:', accuracy, '%\n')
index_list = np.abs(np.array(actual_label_values) - np.array(prob_binary)[:, 0])
wrong_images = []
for p in range(len(actual_labels)):
    if index_list[p] == 1:
        wrong_images.append(actual_labels[p])
wrong_img_paths = []
for i in wrong_images:
    im_path = os.path.join(test_dir, i)
    wrong_img_paths.append(im_path)
for pth in wrong_img_paths:
    if sub_folders[0] in pth:
        os.system("copy " + pth + ' ' + os.path.join(test_failed_dir, sub_folders[0]))
    elif sub_folders[1] in pth:
        os.system("copy " + pth + ' ' + os.path.join(test_failed_dir, sub_folders[1]))
    else:
        print('Wrong case')

training_prediction_set = train_datagen.flow_from_directory(train_dir, target_size=(500, 500), batch_size=batch_size,
                                                            color_mode='rgb', class_mode='binary', shuffle=False)
probabilities = classifier.predict(training_prediction_set)

actual_labels = training_prediction_set.filenames
actual_label_values = []
for label in actual_labels:
    if sub_folders[0] in label:
        actual_label_values.append(0)
    elif sub_folders[0] in label:
        actual_label_values.append(1)
prob_binary = probabilities > 0.5
prob_binary = list(prob_binary)
cm = confusion_matrix(actual_label_values, prob_binary)
accuracy = accuracy_score(actual_label_values, prob_binary)
print('Confusion matrix training set:\n', cm)
print('\nAccuracy on training set:', accuracy, '%\n')
index_list = np.abs(np.array(actual_label_values) - np.array(prob_binary)[:, 0])
wrong_images = []
for p in range(len(actual_labels)):
    if index_list[p] == 1:
        wrong_images.append(actual_labels[p])
wrong_img_paths = []
for i in wrong_images:
    im_path = os.path.join(train_failed_dir, i)
    wrong_img_paths.append(im_path)
for pth in wrong_img_paths:
    if sub_folders[0] in pth:
        os.system("copy " + pth + ' ' + os.path.join(train_failed_dir, sub_folders[0]))
    elif sub_folders[1] in pth:
        os.system("copy " + pth + ' ' + os.path.join(train_failed_dir, sub_folders[1]))
    else:
        print('Wrong case')


def load_image(path=r'', resize_value=(500, 500, 3)):
    im = Image.open(path)
    im_np = np.array(im)
    im_np = im_np / 255.0
    im_np = transform.resize(im_np, resize_value)
    im_np = np.expand_dims(im_np, axis=0)
    return im_np


def predict_probability(model, test_path, image_paths):
    probabilities_list = []
    k = 0
    for p in image_paths:
        path = os.path.join(test_path, p)
        k = k + 1
        print(k, ':', path)
        im_np = load_image(path)
        probabilities_list.append(model.predict(im_np))
    return probabilities_list
