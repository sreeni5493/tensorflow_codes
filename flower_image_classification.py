import tensorflow as tf
import pathlib
import random
import matplotlib.pyplot as plt


AUTOTUNE = tf.data.experimental.AUTOTUNE
URL_PATH = 'https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz'
data_root_orig = tf.keras.utils.get_file(origin=URL_PATH, fname='flower_photos', untar=True)
data_root = pathlib.Path(data_root_orig)
print(data_root)
for item in data_root.iterdir():
    print(item)
all_image_paths = list(data_root.glob('*/*'))
all_image_paths = [str(path) for path in all_image_paths]
random.shuffle(all_image_paths)

image_count = len(all_image_paths)
print('Number of images: ', image_count)

attributions = (data_root/"LICENSE.txt").open(encoding='utf-8').readlines()[4:]
attributions = [line.split(' CC-BY') for line in attributions]
# noinspection PyTypeChecker
attributions = dict(attributions)


def caption_image(img_path):
    image_rel = pathlib.Path(img_path).relative_to(data_root)
    image_rel = str(image_rel)
    image_rel = image_rel.replace('\\', '/')
    return "Image (CC BY 2.0) " + ' - '.join(attributions[image_rel].split(' - ')[:-1])


print('Displaying 3 sample images')


for n in range(3):
    image_path = random.choice(all_image_paths)
    plt.imshow(plt.imread(image_path))
    plt.title(caption_image(image_path))
    plt.show()

label_names = sorted(item.name for item in data_root.glob('*/') if item.is_dir())
print('Label names:', label_names)


label_to_index = dict((name, index) for index, name in enumerate(label_names))
print('Label to index:', label_to_index)

all_image_labels = [label_to_index[pathlib.Path(path).parent.name]
                    for path in all_image_paths]

print("First 10 labels indices: ", all_image_labels[:10])


def preprocess_image(img):
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, [192, 192])
    img /= 255.0  # normalize to [0,1] range

    return img


def load_and_preprocess_image(path):
    img = tf.io.read_file(path)
    return preprocess_image(img)


image_path = all_image_paths[0]
label = all_image_labels[0]
print('Displaying TF image sample resized and pre-processed:', image_path)
plt.imshow(load_and_preprocess_image(image_path))
plt.grid(False)
plt.xlabel(caption_image(image_path))
plt.title(label_names[label].title())
plt.show()

path_ds = tf.data.Dataset.from_tensor_slices(all_image_paths)
image_ds = path_ds.map(load_and_preprocess_image, num_parallel_calls=AUTOTUNE)

print('Displaying images from dataset')
plt.figure(figsize=(8, 8))
for n, image in enumerate(image_ds.take(4)):
    plt.subplot(2, 2, n+1)
    plt.imshow(image)
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plt.xlabel(caption_image(all_image_paths[n]))
plt.show()

label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(all_image_labels, tf.int64))

for label in label_ds.take(10):
    print(label_names[label.numpy()])

image_label_ds = tf.data.Dataset.zip((image_ds, label_ds))
ds = tf.data.Dataset.from_tensor_slices((all_image_paths, all_image_labels))


# The tuples are unpacked into the positional arguments of the mapped function
def load_and_preprocess_from_path_label(path, label):
    return load_and_preprocess_image(path), label


image_label_ds = ds.map(load_and_preprocess_from_path_label)


BATCH_SIZE = 32

# Setting a shuffle buffer size as large as the dataset ensures that the data is
# completely shuffled.
ds = image_label_ds.shuffle(buffer_size=image_count)
ds = ds.repeat()
ds = ds.batch(BATCH_SIZE)
# `prefetch` lets the dataset fetch batches in the background while the model is training.
ds = ds.prefetch(buffer_size=AUTOTUNE)

ds = image_label_ds.apply(
  tf.data.experimental.shuffle_and_repeat(buffer_size=image_count))
ds = ds.batch(BATCH_SIZE)
ds = ds.prefetch(buffer_size=AUTOTUNE)

mobile_net = tf.keras.applications.MobileNetV2(input_shape=(192, 192, 3), include_top=False)
mobile_net.trainable = False


def change_range(image,label):
    return 2*image-1, label


keras_ds = ds.map(change_range)

# The dataset may take a few seconds to start, as it fills its shuffle buffer.
image_batch, label_batch = next(iter(keras_ds))

feature_map_batch = mobile_net(image_batch)
print(feature_map_batch.shape)

model = tf.keras.Sequential([
  mobile_net,
  tf.keras.layers.GlobalAveragePooling2D(),
  tf.keras.layers.Dense(len(label_names), activation='softmax')])

model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss='sparse_categorical_crossentropy',
              metrics=["accuracy"])
steps_per_epoch = tf.math.ceil(len(all_image_paths)/BATCH_SIZE).numpy()
print(type(ds))
print(ds)
model.fit(ds, epochs=10, steps_per_epoch=3)
print('model fitting done')
